package ru.xmn.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import ru.xmn.mvpauth.mvp.views.IView;

/**
 * Created by xmn on 28.10.2016.
 */

public abstract class AbstractPresenter<T extends IView> {
    private T mView;

    public void takeView(T view){
        mView = view;
    }
    public void dropView(){
        mView = null;
    }
    public abstract void initView();

    @Nullable
    public T getView(){
        return mView;
    }
}
