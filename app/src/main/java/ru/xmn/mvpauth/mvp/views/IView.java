package ru.xmn.mvpauth.mvp.views;

/**
 * Created by xmn on 28.10.2016.
 */

public interface IView {
    void showMessage(String message);

    void showError(Throwable error);

    void showLoad();

    void hideLoad();
}
