package ru.xmn.mvpauth.mvp.views;

import java.util.List;

import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;

/**
 * Created by xmn on 29.10.2016.
 */

public interface ICatalogView extends IView {
    void showAddToCartMessage(ProductDto prroductDto);

    void showCatalogView(List<ProductDto> productDtoList);

    void showAuthScreen();

    void updateProductCounter(int count);
}
