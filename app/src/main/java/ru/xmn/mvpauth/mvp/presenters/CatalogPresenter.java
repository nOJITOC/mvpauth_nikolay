package ru.xmn.mvpauth.mvp.presenters;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;
import ru.xmn.mvpauth.mvp.models.CatalogModel;
import ru.xmn.mvpauth.mvp.views.ICatalogView;

/**
 * Created by xmn on 29.10.2016.
 */
public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter {
    private static final String TAG = "CatalogPresenter";
    private CatalogModel mModel;
    private List<ProductDto> mProductDtoList;
    private Set<ProductDto> mBucket = new HashSet<>();//Ведро?)
    private static CatalogPresenter ourInstance = new CatalogPresenter();

    public static CatalogPresenter getInstance() {
        return ourInstance;
    }

    private CatalogPresenter() {
        mModel = new CatalogModel();
    }

    @Override
    public void initView() {
        Log.d(TAG, "initView() called");
        if (mProductDtoList == null) {
            mProductDtoList = mModel.getProductList();
        }

        if (getView() != null) {
            getView().showCatalogView(mProductDtoList);
        }

        updateProductCounter();

    }

    //region icatalogpresenter
    @Override
    public void clickOnBuyButton(int position) {
        if (getView() != null)
            if (checkUserAuth()) {
                addProductToBucket(position);
                getView().showAddToCartMessage(mProductDtoList.get(position));
            } else
                getView().showAuthScreen();
    }

    private void addProductToBucket(int position) {
        mBucket.add(mProductDtoList.get(position));
        updateProductCounter();
    }

    private void updateProductCounter() {
        int bucketCount = 0;
        for (ProductDto p :
                mBucket) {
            bucketCount += p.getCount();
        }
        if (getView() != null) {
            Log.d(TAG, "updateProductCounter() called");
            getView().updateProductCounter(bucketCount);
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mModel.isUserAuth();
    }
    //endregion
}
