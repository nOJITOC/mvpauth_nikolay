package ru.xmn.mvpauth.mvp.views;

import android.support.annotation.Nullable;

import ru.xmn.mvpauth.mvp.presenters.IAuthPresenter;
import ru.xmn.mvpauth.ui.custom_views.AuthPanel;

public interface IAuthView extends IView {

    IAuthPresenter getPresenter();

    @Nullable
    AuthPanel getAuthPanel();

    void setEmailValidation(boolean matches);

    void setPasswordValidation(boolean matches);

    void showCatalogScreen();

    void goBack();

    void updateLoginButton(boolean b);
}
