package ru.xmn.mvpauth.mvp.presenters;

import android.support.annotation.Nullable;

import ru.xmn.mvpauth.mvp.views.IAuthView;

public interface IAuthPresenter {

    void takeView(IAuthView authView);

    void initView();

    @Nullable
    IAuthView getView();

    void clickOnLogin();

    void clickOnFb();

    void clickOnVk();

    void clickOnTwitter();

    void clickOnShowCatalog();

    boolean checkUserAuth();

    void checkEmail(String email);

    void checkPassword(String password);
}
