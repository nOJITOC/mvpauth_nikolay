package ru.xmn.mvpauth.mvp.presenters;

import java.util.HashMap;
import java.util.Map;

import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;

/**
 * Created by xmn on 29.10.2016.
 */

public class ProductPresenterFactory {
    private static final Map<String, ProductPresenter> sPresenterMap = new HashMap<String, ProductPresenter>();

    public static ProductPresenter getInstance(ProductDto productDto){
        ProductPresenter presenter = sPresenterMap.get(String.valueOf(productDto.getId()));
        if (presenter==null){
            presenter = ProductPresenter.newInstance(productDto);
            registerPesenter(productDto, presenter);
        }
        return presenter;
    }

    private static void registerPesenter(ProductDto productDto, ProductPresenter presenter) {
        sPresenterMap.put(String.valueOf(productDto.getId()), presenter);
    }
}
