package ru.xmn.mvpauth.mvp.presenters;

import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;
import ru.xmn.mvpauth.mvp.models.ProductModel;
import ru.xmn.mvpauth.mvp.views.IProductView;

/**
 * Created by xmn on 29.10.2016.
 */
public class ProductPresenter extends AbstractPresenter<IProductView> implements IProductPresenter {
    private static final String TAG = "ProductPresenter";
    private ProductModel mProductModel;
    private ProductDto mProduct;

    static ProductPresenter newInstance(ProductDto prod) {
        return new ProductPresenter(prod);
    }

    private ProductPresenter(ProductDto prod) {
        mProduct = prod;
        mProductModel = new ProductModel();
    }

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProduct);
        }
    }

    //region iproductpresenter
    @Override
    public void clickOnPlus() {
        if (mProduct.getCount()>0) {
            mProduct.addProduct();
            mProductModel.updateProduct(mProduct);
            if (getView() != null) {
                getView().updateProductCountView(mProduct);
            }
        }
    }

    @Override
    public void clickOnMinus() {
        if (mProduct.getCount()>1) {
            mProduct.deleteProduct();
            mProductModel.updateProduct(mProduct);
            if (getView() != null) {
                getView().updateProductCountView(mProduct);
            }
        }
    }
    //endregion
}
