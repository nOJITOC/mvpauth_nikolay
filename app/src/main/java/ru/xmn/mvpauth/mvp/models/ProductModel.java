package ru.xmn.mvpauth.mvp.models;

import java.util.ArrayList;
import java.util.List;

import ru.xmn.mvpauth.mvp.data.DataManager;
import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;

/**
 * Created by xmn on 28.10.2016.
 */

public class ProductModel {
    DataManager mDataManager = DataManager.getInstance();

    public ProductDto getProductById (int id) {
        // TODO: 28.10.2016 get prod from datamanager
        return mDataManager.getProductById(id);
    }

    public void updateProduct(ProductDto product){
        mDataManager.updateProduct(product);
    }

}
