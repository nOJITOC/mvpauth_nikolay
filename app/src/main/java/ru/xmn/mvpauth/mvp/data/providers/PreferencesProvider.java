package ru.xmn.mvpauth.mvp.data.providers;

import android.content.SharedPreferences;

import ru.xmn.mvpauth.utils.MvpAuthApp;

public class PreferencesProvider {
    private SharedPreferences mSharedPreferences;
    private final String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";

    public PreferencesProvider() {
        this.mSharedPreferences = MvpAuthApp.getSharedPreferences();
    }

    public void saveAuthToken(String authToken) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(AUTH_TOKEN_KEY, authToken);
        editor.apply();
    }

    public String getAuthToken() {
        return mSharedPreferences.getString(AUTH_TOKEN_KEY, "null");
    }
}
