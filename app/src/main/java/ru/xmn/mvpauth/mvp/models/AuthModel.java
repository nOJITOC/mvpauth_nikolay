package ru.xmn.mvpauth.mvp.models;

import ru.xmn.mvpauth.mvp.data.DataManager;
import ru.xmn.mvpauth.mvp.data.providers.AuthProvider;
import ru.xmn.mvpauth.mvp.data.providers.PreferencesProvider;

public class AuthModel {
    private DataManager mDataManager = DataManager.getInstance();

    public AuthModel() {
    }

    public boolean isAuthUser() {
        return mDataManager.isUserAuth();
    }

    public void loginUser(String email, String password) {
        mDataManager.loginUser(email, password);
    }
}
