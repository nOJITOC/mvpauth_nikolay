package ru.xmn.mvpauth.mvp.data;

import java.util.ArrayList;
import java.util.List;

import ru.xmn.mvpauth.mvp.data.providers.AuthProvider;
import ru.xmn.mvpauth.mvp.data.providers.PreferencesProvider;
import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;

/**
 * Created by xmn on 28.10.2016.
 */
public class DataManager {
    private List<ProductDto> mMockProductList;
    private static DataManager ourInstance = new DataManager();
    private PreferencesProvider mPreferencesProvider;
    private AuthProvider mAuthProvider;

    public static DataManager getInstance() {
        return ourInstance;
    }

    private DataManager() {
        mPreferencesProvider = new PreferencesProvider();
        mAuthProvider = new AuthProvider();
        generateMockData();
    }

    public ProductDto getProductById(int id) {
        return mMockProductList.get(id + 1);
        // TODO: 28.10.2016 sample mock data from bd
    }

    public void updateProduct(ProductDto product) {
        // TODO: 28.10.2016 upd prod count and status, save in DB
    }

    public List<ProductDto> getProductList() {
        // TODO: 28.10.2016 load product list
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDto(1, "KIMBO EXTRA CREAM",
                "http://www.italco.ru/upload/iblock/e36/e362b7ec4389f848a818012176992a9c.jpg",
                "Великолепный баланс сладости и легкой кислинки.",
                500, 1));
        mMockProductList.add(new ProductDto(2, "SMART СС ROMA",
                "http://www.italco.ru/upload/iblock/78c/78cd51369455f8762077813876341cff.png",
                "Крепкая  смесь эспрессо  из лучших зерен Арабики и Робусты",
                500, 1));
        mMockProductList.add(new ProductDto(3, "LAVAZZA ORO",
                "http://www.lavazzashop.ru/cofimages/postimg/images/Lavazza-Qualita-Oro_bg.jpg",
                "смесь лучших сортов Арабики из Центральной и Южной Америки.",
                500, 1));
        mMockProductList.add(new ProductDto(4, "LAVAZZA CREMA AROMA",
                "http://www.italco.ru/upload/iblock/e5c/e5c9bf03aa75373f4d73532a20ccbf72.jpg",
                "одна из самых популярных смесей лучших сортов Арабики",
                500, 1));
    }

    public boolean isUserAuth() {
        return mPreferencesProvider.getAuthToken()!=null;
    }

    public void loginUser(String email, String password) {
        String authToken = mAuthProvider.authorize(email, password);
        mPreferencesProvider.saveAuthToken(authToken);
    }
}
