package ru.xmn.mvpauth.mvp.presenters;

import android.util.Log;

import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.mvp.models.AuthModel;
import ru.xmn.mvpauth.mvp.views.IAuthView;
import ru.xmn.mvpauth.ui.custom_views.AuthPanel;
import ru.xmn.mvpauth.utils.MvpAuthApp;

public class AuthPresenter extends AbstractPresenter<IAuthView> implements IAuthPresenter {
    private static final String TAG = "AuthPresenter";
    private static AuthPresenter Instance = new AuthPresenter();
    private AuthModel mAuthModel;

    private AuthPresenter() {
        mAuthModel = new AuthModel();
    }

    public static AuthPresenter getInstance() {
        return Instance;
    }

    @Override
    public void initView() {
        Log.d(TAG, "initView() called " + getView());
        if (getView() != null) {
                getView().updateLoginButton(checkUserAuth());
        }
    }

    //region iauthpresenter
    @Override
    public void clickOnLogin() {
        //большая вложенность if, можно разделить на функции разной степени абстракции
        if (getView() != null && getView().getAuthPanel() != null) {
            if (getView().getAuthPanel().isIdle()) {
                getView().getAuthPanel().setCustomState(AuthPanel.LOGIN_STATE);
            } else {
                String email = getView().getAuthPanel().getUserEmail();
                String password = getView().getAuthPanel().getUserPassword();
                if (isValidEmail(email)&&isValidPassword(password)) {
                    mAuthModel.loginUser(email, password);
                    getView().showMessage("Вход выполнен");
                    getView().getAuthPanel().setCustomState(AuthPanel.IDLE_STATE);
                    getView().updateLoginButton(checkUserAuth());
                } else {
                    getView().showMessage(MvpAuthApp.get().getResources().getString(R.string.incorrect_login_data));
                }
            }
        }
    }

    @Override
    public void clickOnFb() {
        if (getView() != null) {
            getView().showMessage("click_fb_mock");
        }
    }

    @Override
    public void clickOnVk() {
        if (getView() != null) {
            getView().showMessage("click_vk_mock");
        }
    }

    @Override
    public void clickOnTwitter() {
        if (getView() != null) {
            getView().showMessage("click_twitter_mock");
        }
    }

    @Override
    public void clickOnShowCatalog() {
        Log.d(TAG, "clickOnShowCatalog() called " + getView());
        if (getView() != null) {
            getView().showMessage("show_catalog_mock");
            // TODO: 27.10.2016 if updata data complete start catalog screen
            getView().showCatalogScreen();
        }
    }

    @Override
    public boolean checkUserAuth() {
        return mAuthModel.isAuthUser();
    }
    //endregion

    //region check validation
    public void checkEmail(String email) {
        if (email != null && getView()!=null) {
            getView().setEmailValidation(isValidEmail(email));
        }
    }

    private boolean isValidEmail(String email) {
        String EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        return email.matches(EMAIL_REGEX);
    }

    public void checkPassword (String password) {
        if (password != null && getView()!=null) {
            getView().setPasswordValidation(isValidPassword(password));
        }
    }

    private boolean isValidPassword(String password) {
        int MIN_PASSWORD_LENGTH = 8;
        return password.length() >= MIN_PASSWORD_LENGTH;
    }
    //endregion

}
