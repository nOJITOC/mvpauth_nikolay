package ru.xmn.mvpauth.utils;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class MvpAuthApp extends Application {
    public static SharedPreferences sSharedPreferences;
    private static Application instance;
    public static Application get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }
}
