package ru.xmn.mvpauth.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import ru.xmn.mvpauth.BuildConfig;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.mvp.presenters.AuthPresenter;
import ru.xmn.mvpauth.mvp.presenters.IAuthPresenter;
import ru.xmn.mvpauth.mvp.views.IAuthView;
import ru.xmn.mvpauth.mvp.views.IView;
import ru.xmn.mvpauth.ui.custom_views.AuthPanel;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.xmn.mvpauth.ui.fragments.AuthFragment;

public class SplashActivity extends AppCompatActivity implements IView {
    private static final String TAG = "SplashActivity";
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.splash_fragment_container)
    FrameLayout mFragmentContainer;

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume() called");
//        refreshAuthFragment();не стоит осталять закоментированный код
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        Log.d(TAG, "onCreate() called with: savedInstanceState = [" + savedInstanceState + "]");
        refreshAuthFragment();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void showError(Throwable error) {
        if (BuildConfig.DEBUG) {
            showMessage(error.getMessage());
            error.printStackTrace();
        } else {
            showMessage("error");
            // TODO: 20.10.2016 sent to crashlytics
        }
    }

    //region iview
    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLoad() {
        if (mProgressBar.getVisibility() == View.GONE)
            mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoad() {
        if (mProgressBar.getVisibility() == View.VISIBLE)
            mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        //хорошее решение, только нужно не здесь, а в RootActivity(при нахождении в каталоге товаров)
        new AlertDialog.Builder(this)
                .setTitle("Выход")
                .setMessage("Вы уверены что хотите закрыть приложение?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, (arg0, arg1) -> SplashActivity.super.onBackPressed())
                .create().show();
    }

    //endregion

    private void refreshAuthFragment() {
        AuthFragment fragment = (AuthFragment) getSupportFragmentManager().findFragmentById(R.id.splash_fragment_container);
        Log.d(TAG, "refreshAuthFragment() called " + fragment);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.splash_fragment_container, fragment==null? new AuthFragment():fragment)
                .commit();
        getSupportFragmentManager().executePendingTransactions();
    }

}
