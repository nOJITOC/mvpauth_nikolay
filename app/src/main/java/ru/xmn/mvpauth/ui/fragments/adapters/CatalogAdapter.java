package ru.xmn.mvpauth.ui.fragments.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;
import ru.xmn.mvpauth.ui.fragments.ProductFragment;

/**
 * Created by xmn on 29.10.2016.
 */

public class CatalogAdapter extends FragmentPagerAdapter {
    private List<ProductDto> mProductList = new ArrayList<>();

    public CatalogAdapter (FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ProductFragment.newInstance(mProductList.get(position));
    }

    @Override
    public int getCount() {
        return mProductList.size();
    }

    public void addItem(ProductDto product){
        mProductList.add(product);
        notifyDataSetChanged();
    }
}
