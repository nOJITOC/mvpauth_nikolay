package ru.xmn.mvpauth.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.xmn.mvpauth.BuildConfig;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.mvp.presenters.AuthPresenter;
import ru.xmn.mvpauth.mvp.presenters.IAuthPresenter;
import ru.xmn.mvpauth.mvp.views.IAuthView;
import ru.xmn.mvpauth.mvp.views.IView;
import ru.xmn.mvpauth.ui.activities.RootActivity;
import ru.xmn.mvpauth.ui.custom_views.AuthPanel;

/**
 * Created by xmn on 30.10.2016.
 */

public class AuthFragment extends Fragment implements IAuthView, View.OnClickListener {
    private static final String TAG = "AuthFragment";
    public static int PARENT_AUTH = 0;
    public static int PARENT_ROOT = 1;
    Activity mActivity;

    int currentParent = 0;

    IAuthPresenter mPresenter = AuthPresenter.getInstance();

    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.auth_wrapper)
    AuthPanel mAuthPanel;
    private Context mContext;

    public void setParentId(int parentId) {
        currentParent = parentId;
    }

    //region lifecycle
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auth, container, false);
        ButterKnife.bind(this, view);

        setRetainInstance(true);

        if (currentParent==PARENT_ROOT) {
            mAuthPanel.setCustomState(AuthPanel.LOGIN_WITH_BACK_STATE, false);
        }

        Log.d(TAG, "onCreateView() called with: inflater = [" + this + "],");
        mPresenter.takeView(this);
        mPresenter.initView();

        mLoginBtn.setOnClickListener(this);
        mShowCatalogBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach() called with: context = [" + context + "]");
        mContext = context;
        super.onAttach(context);
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
    //endregion

    @Override
    public IAuthPresenter getPresenter() {
        return mPresenter;
    }

    //region iview
    @Override
    public void showMessage(String message) {
        IView iView = (IView) mActivity;
        iView.showMessage(message);
    }

    @Override
    public void showError(Throwable error) {
        if (BuildConfig.DEBUG) {
            showMessage(error.getMessage());
            error.printStackTrace();
        } else {
            showMessage("error");
            // TODO: 20.10.2016 sent to crashlytics
        }
    }

    @Override
    public void showLoad() {
    }

    @Override
    public void hideLoad() {
    }
    //endregion

    //region iauthview
    @Override
    public AuthPanel getAuthPanel() {
        return mAuthPanel;
    }

    @Override
    public void setEmailValidation(boolean matches) {
        if (matches) {
            mAuthPanel.getEmailWrap().setErrorEnabled(false);
        } else {
            mAuthPanel.getEmailWrap().setError(getResources().getString(R.string.incorrect_email));
        }
    }
    @Override
    public void setPasswordValidation(boolean matches) {
        if (matches) {
            mAuthPanel.getPasswordWrap().setErrorEnabled(false);
        } else {
            mAuthPanel.getPasswordWrap().setError(getResources().getString(R.string.incorrect_password_length));
        }
    }

    @Override
    public void showCatalogScreen() {
        Intent intent = new Intent(mContext, RootActivity.class);
        mContext.startActivity(intent);
    }

    @Override
    public void goBack() {
        Log.d(TAG, "goBack() called");
        getActivity().onBackPressed();
    }

    @Override
    public void updateLoginButton(boolean b) {
        mAuthPanel.updateLoginButton(b);
    }
    //endregion

    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick() called with: view = [" + view + "]");
        switch (view.getId()) {
            case R.id.show_catalog_btn:
                int customState = getAuthPanel().getCustomState();
                if (customState == AuthPanel.LOGIN_WITH_BACK_STATE) {
                    goBack();
                } else {
                    mPresenter.clickOnShowCatalog();
                }
                break;
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
        }
    }

}
