package ru.xmn.mvpauth.ui.activities;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.mvp.data.DataManager;
import ru.xmn.mvpauth.mvp.views.IView;
import ru.xmn.mvpauth.ui.fragments.AccountFragment;
import ru.xmn.mvpauth.ui.fragments.AuthFragment;
import ru.xmn.mvpauth.ui.fragments.CatalogFragment;
import ru.xmn.mvpauth.ui.fragments.ProductFragment;
import ru.xmn.mvpauth.ui.ui_utils.CropCircleTransformation;

public class RootActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, IView {
    private static final String TAG = "RootActivity";
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @BindView(R.id.nav_view)
    NavigationView mNavigationView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.fragment_container)
    FrameLayout mFragmentContainer;
    FragmentManager mFragmentManager;
    @BindView(R.id.root_coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    private TextView mBucketCountView;
    private int mBucketInt = 0;

    //region IView
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(Throwable error) {

    }

    @Override
    public void showLoad() {

    }

    @Override
    public void hideLoad() {

    }

    //endregion

    //region activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);

        initToolbar();
        initDrawer();

        mFragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new CatalogFragment())
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        MenuItem item = menu.findItem(R.id.badge);
        MenuItemCompat.setActionView(item, R.layout.bucket_layout);
        RelativeLayout notifCount = (RelativeLayout) MenuItemCompat.getActionView(item);

        mBucketCountView = (TextView) notifCount.findViewById(R.id.actionbar_notifcation_textview);
        updateProductCounter(mBucketInt);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.nav_account:
                fragment = new AccountFragment();
                break;
            case R.id.nav_catalog:
                fragment = new CatalogFragment();
                break;
            case R.id.nav_favorite:
                fragment = new AccountFragment();
                break;
            case R.id.nav_orders:
                fragment = new AccountFragment();
                break;
            case R.id.nav_notification:
                fragment = new AccountFragment();
                break;
        }
        if (fragment!=null){
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
            item.setChecked(true);
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return false;
    }
    //endregion

    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
        ImageView userAvatar = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.user_avatar);
        Glide.with(this).load(R.drawable.boyar_img).asBitmap().centerCrop()
                .transform(new CropCircleTransformation(this)).into(userAvatar);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    public void replaceFragment(Fragment fragment) {
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void updateProductCounter(int count) {
        Log.d(TAG, "updateProductCounter() called with: count = [" + count + "]");
        if (mBucketCountView!=null) {
            if (count>0){
                mBucketCountView.setVisibility(View.VISIBLE);
                mBucketCountView.setText(String.valueOf(count));
            } else {
                mBucketCountView.setVisibility(View.GONE);
            }
        }else mBucketInt = count;

    }

    public void setCheckedItem(int checkedItem) {
        mNavigationView.getMenu().findItem(checkedItem).setChecked(true);
    }
}
