package ru.xmn.mvpauth.ui.custom_views;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.LinearLayout;

import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.mvp.presenters.AuthPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class AuthPanel extends LinearLayout {
    private static final String TAG = "AuthPanel";
    private static final long ANIMATION_DURATION = 300;
    public static final int LOGIN_STATE = 0;
    public static final int IDLE_STATE = 1;
    public static final int LOGIN_WITH_BACK_STATE = 2;
    private static final String LOGIN_TEXT = "Войти";
    private static final String RELOGIN_TEXT = "Войти другим пользователем";
    private int mCustomState = 1;

    private AuthPresenter mAuthPresenter;

    @BindView(R.id.auth_card)
    CardView mAuthCard;
    @BindView(R.id.login_btn)
    Button mLoginBtn;
    @BindView(R.id.show_catalog_btn)
    Button mShowCatalogBtn;
    @BindView(R.id.login_email_et)
    TextInputEditText mLoginEmailEt;
    @BindView(R.id.login_password_et)
    TextInputEditText mLoginPasswordEt;
    @BindView(R.id.login_email_wrap)
    TextInputLayout mEmailWrap;
    @BindView(R.id.login_password_wrap)
    TextInputLayout mPasswordWrap;

    public AuthPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        mAuthPresenter = AuthPresenter.getInstance();
    }

    public TextInputLayout getPasswordWrap() {
        return mPasswordWrap;
    }

    public TextInputLayout getEmailWrap() {
        return mEmailWrap;
    }

    //region bind fields rx
    //не очень информативное название
    private void bindRx() {
        Observable<String> emailObservable = RxTextView.textChanges(mLoginEmailEt).compose(transformValidationCharSeq);
        Observable<String> passwordObservable = RxTextView.textChanges(mLoginPasswordEt).compose(transformValidationCharSeq);

        emailObservable.observeOn(AndroidSchedulers.mainThread()).subscribe(mAuthPresenter::checkEmail);
        passwordObservable.observeOn(AndroidSchedulers.mainThread()).subscribe(mAuthPresenter::checkPassword);
    }

    Observable.Transformer<CharSequence, String> transformValidationCharSeq = obs -> obs
            .debounce(500, TimeUnit.MILLISECONDS)
            .map(CharSequence::toString)
            .filter(s -> s.length() > 0);
    //endregion

    //region state
    public void setCustomState(int customState) {
        setCustomState(customState, true);
    }

    public void setCustomState(int customState, boolean animate) {
        Log.d(TAG, "setCustomState() called with: customState = [" + customState + "], animate = [" + animate + "]");
        if (animate) {
            resizeCard();//реализацию бы поближе, прыгать не очень удобно(хотя студия и позволяет это сделать быстро)
        }
        mCustomState = customState;
        showViewFromState();
    }

    private void showViewFromState() {
        switch (mCustomState) {
            case LOGIN_STATE:
                showLoginState();
                break;
            case IDLE_STATE:
                showIdleState();
                break;
            case LOGIN_WITH_BACK_STATE:
                showLoginWithBackState();
                break;
        }
    }

    private void showLoginState() {
        Log.d(TAG, "showLoginState() called");
        mAuthCard.setVisibility(VISIBLE);
        mLoginBtn.setText(LOGIN_TEXT);
        mShowCatalogBtn.setVisibility(GONE);
    }

    private void showLoginWithBackState() {
        Log.d(TAG, "showLoginWithBackState() called");
        mAuthCard.setVisibility(VISIBLE);
        mShowCatalogBtn.setVisibility(VISIBLE);
        mShowCatalogBtn.setText("ВЕРНУТЬСЯ В КАТАЛОГ");
    }

    private void showIdleState() {
        Log.d(TAG, "showIdleState() called");
        mAuthCard.setVisibility(GONE);
        mShowCatalogBtn.setVisibility(VISIBLE);
    }
    //endregion

    //region lifecycle
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        showViewFromState();
        Log.d(TAG, "onFinishInflate() called");
        bindRx();
    }
    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.state = mCustomState;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(state);
        Log.d(TAG, "onRestoreInstanceState() called with: state = [" + savedState.state + "]");
        setCustomState(savedState.state, false);
    }

    //endregion

    //region getters
    public int getCustomState() {return mCustomState;}

    public String getUserEmail() {
        return String.valueOf(mLoginEmailEt.getText());
    }

    public String getUserPassword() {
        return String.valueOf(mLoginPasswordEt.getText());
    }
    //endregion

    public boolean isIdle() {
        return mCustomState == IDLE_STATE;
    }

    private void resizeCard() {
        Animation animation = new ScaleAnimation(
                1.0f, 1.0f,
                mCustomState==IDLE_STATE?0.3f:1.0f, mCustomState==IDLE_STATE?1.0f:0.3f,
                Animation.RELATIVE_TO_SELF, 1.0f,
                Animation.RELATIVE_TO_SELF, 1.0f
        );
        animation.setDuration(ANIMATION_DURATION);
        mAuthCard.clearAnimation();
        mAuthCard.startAnimation(animation);
    }

    public void updateLoginButton(boolean b) {
        if (b)
            mLoginBtn.setText(RELOGIN_TEXT);
        else
            mLoginBtn.setText(LOGIN_TEXT);
    }

    static class SavedState extends BaseSavedState {
        private int state;

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            state = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(state);
        }

    }
}
