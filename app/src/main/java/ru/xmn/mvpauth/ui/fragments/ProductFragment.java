package ru.xmn.mvpauth.ui.fragments;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.xmn.mvpauth.R;
import ru.xmn.mvpauth.mvp.data.storage.dto.ProductDto;
import ru.xmn.mvpauth.mvp.presenters.ProductPresenter;
import ru.xmn.mvpauth.mvp.presenters.ProductPresenterFactory;
import ru.xmn.mvpauth.mvp.views.IProductView;
import ru.xmn.mvpauth.ui.activities.RootActivity;

/**
 * Created by xmn on 29.10.2016.
 */

public class ProductFragment extends Fragment implements IProductView, View.OnClickListener {
    @BindView(R.id.product_name_txt)
    TextView productNameTxt;
    @BindView(R.id.product_description_txt)
    TextView productDescriptionTxt;
    @BindView(R.id.product_image)
    ImageView productImage;
    @BindView(R.id.product_count_txt)
    TextView productCountTxt;
    @BindView(R.id.product_price_txt)
    TextView productPrice;
    @BindView(R.id.minus_btn)
    ImageView minusBtn;
    @BindView(R.id.plus_btn)
    ImageView plusBtn;

    private ProductPresenter mPresenter;

    //region new instance + bundle read
    public static ProductFragment newInstance(ProductDto product){
        Bundle bundle = new Bundle();
        bundle.putParcelable("PRODUCT", product);
        ProductFragment productFragment = new ProductFragment();
        productFragment.setArguments(bundle);
        return productFragment;
    }

    private void readBundle(Bundle bundle){
        if (bundle != null) {
            ProductDto product = bundle.getParcelable("PRODUCT");
            mPresenter = ProductPresenterFactory.getInstance(product);
        }
    }
    //endregion

    //region lifecycle
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        readBundle(getArguments());
        mPresenter.takeView(this);
        mPresenter.initView();
        plusBtn.setOnClickListener(this);
        minusBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }
    //endregion

    //region iproductview
    @Override
    public void showProductView(ProductDto product) {
        productCountTxt.setText(String.valueOf(product.getCount()));
        productDescriptionTxt.setText(product.getDescription());
        productNameTxt.setText(product.getProductName());
        if (product.getCount()>0) {
            productPrice.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        }
        Glide.with(getContext())
                .load(product.getImageUrl())
                .into(productImage);
    }

    @Override
    public void updateProductCountView(ProductDto product) {
        productCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount()>0) {
            productPrice.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        }
    }
    //endregion

    //region IVIEW
    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable error) {
        getRootActivity().showError(error);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }
    //endregion

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.plus_btn:
                mPresenter.clickOnPlus();
                break;
            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
        }
    }
}
